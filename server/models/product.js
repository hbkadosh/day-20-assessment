// product.js
// Creates a model for grocery_list table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (conn, Sequelize) {
    var Product = conn.define('product', {
            id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true
            },
            upc12: {
                type: Sequelize.BIGINT(12),
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING(255),
                allowNull: false
            },
            name: {
                type: Sequelize.STRING(255),
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "grocery_list"
        }
    );

    return Product;
};