// Defines client-side routing
(function () {
    angular
        .module("GSS")
        .config(pdtRouteConfig)
    pdtRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function pdtRouteConfig($stateProvider, $urlRouterProvider) {
        
        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'   
            })
            .state('edit', {
                url: '/edit',
                parent: 'search',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })
            // .state('editWithParam', {
            //     url: '/edit/:pdtNo',
            //     templateUrl: './app/edit/edit.html',
            //     controller: 'EditCtrl',
            //     controllerAs: 'ctrl'
            // })
            // .state('register', {
            //     url: '/register',
            //     templateUrl: './app/registration/register.html',
            //     controller: 'RegCtrl',
            //     controllerAs: 'ctrl'
            // })
            
            // .state('searchDB', {
            //     url: '/searchDB',
            //     templateUrl: './app/search/searchDB.html',
            //     controller: 'SearchDBCtrl',
            //     controllerAs: 'ctrl'
            // })
            // .state('thanks', {
            //     url: '/thanks',
            //     templateUrl: './app/registration/thanks.html'
            // })

        $urlRouterProvider.otherwise("/index");
    }
})();

