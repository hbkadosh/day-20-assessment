(function () {
    'use strict';
    angular
        .module("GSS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "$stateParams", "PdtService"];

    function EditCtrl($filter, $stateParams, PdtService) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.name = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        // vm.deleteManager = deleteManager;
        vm.initDetails = initDetails;
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        // vm.updateDeptName = updateDeptName;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();
        console.log("before state");
        if($stateParams.deptNo){
            console.log("$stateparams " + $stateParams.deptNo);
            vm.dept_no = $stateParams.deptNo;
            vm.search();
        }


        // Function declaration and definition -------------------------------------------------------------------------
        // Deletes displayed manager. Details of preceding manager is then displayed.
        function deleteManager() {
            PdtService
                .deleteDept(vm.dept_no, vm.result.manager_id)
                .then(function (response) {
                    // Calls search() in order to populate manager info with predecessor of deleted manager
                    search();
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error: \n" + JSON.stringify(err));
                });
        }

        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.name = "";
            vm.result.brand = "";
            vm.result.id = "";
            vm.result.upc12 = "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        }

        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
//            initDetails();
            vm.showDetails = true;

            PdtService
                .retrievePdtByName(vm.name)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("CTRL Edit > retrievePdtByName() > results: " + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.name = result.data.name;
                    vm.result.brand = result.data.brand;
                })
                .catch(function (err) {
                    console.log("CTRL Edit >> retrievePdtByName() Error: " + JSON.stringify(err));
                });
        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

        // Saves edited department name
        function updatePdtBrand() {
            console.log("CTRL Edit >> updatePdtBrand()");
            PdtService
                .updatePdt(vm.name, vm.result.brand)
                .then(function (result) {
                    console.log("CTRL Edit >> updatePdtBrand() results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }
    }
})();