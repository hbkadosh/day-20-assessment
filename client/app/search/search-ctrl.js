
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches a SearchCtrl to the DMS module
    angular
        .module("GSS")
        .controller("SearchCtrl", SearchCtrl);

    // Dependency injection. An empty [] means RegCtrl does not have dependencies. Here we inject PdtService so
    // RegCtrl can call services related to department.
    // Dependency injection. An empty [] means SearchCtrl does not have dependencies
    SearchCtrl.$inject = ['PdtService'];

    // Search function declaration
    function SearchCtrl(PdtService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the SearchCtrl). Any
        // function or variable that you attach to vm will be exposed to callers of SearchCtrl, e.g., search.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        vm.products = [];
        vm.searchString = '';
        console.log("CTRL Search >> Str: ", vm.searchString);

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view. Currently, search.controller.js doesn't have any exposed
        // functions
        vm.search = search;

        // vm.EditCtrl = EditCtrl;

        // function EditCtrl() {
        //     $state.go('edit');
        // }

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        // init();

        // Function declaration and definition -------------------------------------------------------------------------
        // The init function initializes view
        function init() {

            PdtService
                .retrievePdt(vm.searchString)
                .then(function(results){
                    console.log("CTRL Search >> init() results: ", results);
                    vm.products = results.data;
                })
                .catch(function(err){
                    console.log("error " + err);
                });
        }

        function search() {
            vm.showProduct = false;
            console.log("CTRL Search >> search() searchStr: ", vm.searchString);
            PdtService
                .retrievePdt(vm.searchString)
                .then(function (results) {
                    console.log("CTRL Search >> search() results: ", results);
                    vm.products = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

    }
})();